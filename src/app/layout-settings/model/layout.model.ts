export class LayoutModel {
	tweetNumber: number;
	initDate: string;
	endDate: string;

	constructor(tNumber?: number) {
		tNumber ? this.tweetNumber = tNumber : this.tweetNumber = 30;
		this.initDate = "";
		this.endDate = "";

	}
}
