import { Component, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-layout-settings',
  templateUrl: './layout-settings.component.html',
  styleUrls: ['./layout-settings.component.css']
})
export class LayoutSettingsComponent {

  @Output() doAccept: EventEmitter<Object> = new EventEmitter();

  constructor(public modal: NgbActiveModal) {

  }


  onAccept() {
  	this.doAccept.emit('clicking this modal');
  }

}
