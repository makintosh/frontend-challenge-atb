export class UserModel {
	id: string;
	name: string;
	tweets: Array<any>;
	counter: number;

	constructor(identifier: string, name: string) {
		this.id = identifier;
		this.name = name;
		this.tweets = new Array();
		this.counter = 30;
	}
}
