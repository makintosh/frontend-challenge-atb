import { Component, OnInit } from '@angular/core';
import { WsService } from '../services/ws.service';
import { UserModel } from './model/user.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LayoutSettingsComponent } from '../layout-settings/layout-settings.component';

const MODALS = {
  layoutModal: LayoutSettingsComponent
};

@Component({
  selector: 'app-tweets-page',
  templateUrl: './tweets-page.component.html',
  styleUrls: ['./tweets-page.component.css']
})
export class TweetsPageComponent implements OnInit {

  arrUser: Array<UserModel> = new Array();
  // layoutModal: LayoutSettingsComponent;

  constructor(private ws: WsService, private _modalService: NgbModal) {
  	this.arrUser.push(
  		new UserModel('@makeschool', 'makeschool'),
  		new UserModel('@newsycombinator', 'newsycombinator'),
  		new UserModel('@le_roi_lezard', 'le_roi_lezard'));
  	// this.layoutModal = new LayoutSettingsComponent();
  	// new UserModel('@ycombinator', 'ycombinator')
  }

  ngOnInit() {
  	this.getTweets().then(res => {
  		console.log('under get tweets');
  		if (res) {
  			console.log('printing users: ', this.arrUser);
  		}
  	});
  }

  getTweets() {
  	const promise = new Promise((resolve, reject) => {
  		console.log('under promise');
  		this.arrUser.forEach((user, uIdx, uArr) => {
	  		this.ws.getTweetsFromUser(user.name).then(response => {
		  		user.tweets = Object.values(response);
		  		uIdx === uArr.length - 1 ? resolve(true) : console.log('not the end with' + user.name);
		  	});
	  	});
  	});
  	return promise;
  }

  openModal(name: string) {
    this._modalService.open(MODALS[name]);
  }

}
