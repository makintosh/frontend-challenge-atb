import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { WsService } from './services/ws.service';

import { AppComponent } from './app.component';
import { TweetsPageComponent } from './tweets-page/tweets-page.component';
import { LayoutSettingsComponent } from './layout-settings/layout-settings.component';

@NgModule({
  declarations: [
    AppComponent,
    TweetsPageComponent,
    LayoutSettingsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [WsService],
  entryComponents: [LayoutSettingsComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
