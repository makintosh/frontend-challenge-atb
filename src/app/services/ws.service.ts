import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LayoutModel } from '../layout-settings/model/layout.model';

@Injectable()
export class WsService {
	constructor(private http: HttpClient) {	}

	/**
	* Get the tweets from user
	* @param username
	*/
	getTweetsFromUser(username: string) {
		return new Promise((resolve, reject) => {
			this.http.get('http://localhost:7890/1.1/statuses/user_timeline.json?count=30&screen_name=' + username)
			.toPromise().then(data => {
				resolve(data);
			});
		});
	}
}
